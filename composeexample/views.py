from django.http import HttpResponse
import time
import html

def saludo(request):
    tiempo = time.strftime("%c")
    imagen = request.GET['imagen']
    nombre = request.GET['nombre']
    dpi = request.GET['dpi']
        
    data = """
        <center>
            <hr width = "250">
            <h2>FECHA & HORA: %s</h2>
            <img src= %s height = "150" width = "150">
            <h2>NOMBRE: %s</h2>
            <h2>DPI: %s</h2>
            <hr width = "250">
        </center>
    """%(tiempo, imagen, nombre, dpi)

    return HttpResponse(data, content_type='text/html')

    #http://localhost:8000/welcome/?imagen=https://static.thenounproject.com/png/17241-200.png&nombre=CarlosLuna&dpi=2931413571705
