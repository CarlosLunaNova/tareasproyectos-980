from django.contrib import admin
from registro.models import RegistroMercantil

# Register your models here.

@admin.register(RegistroMercantil)

class RegistroMercantilAdmin(admin.ModelAdmin):
    list_display = ('usuario','telefono','empresa','departamento','municipio','creacion','modificacion')

