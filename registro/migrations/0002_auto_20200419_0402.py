# Generated by Django 2.2.11 on 2020-04-19 04:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('registro', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RegistroMercantil',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('edad', models.CharField(max_length=2)),
                ('telefono', models.CharField(max_length=8)),
                ('estado_civil', models.CharField(max_length=10)),
                ('nacionalidad', models.CharField(max_length=15)),
                ('profesion', models.CharField(max_length=30)),
                ('departamento', models.CharField(max_length=30)),
                ('municipio', models.CharField(max_length=30)),
                ('dpi', models.CharField(max_length=13)),
                ('nit', models.CharField(max_length=10)),
                ('email', models.CharField(blank=True, max_length=15, null=True)),
                ('empresa', models.CharField(max_length=30)),
                ('descripcion', models.CharField(max_length=50)),
                ('creacion', models.DateTimeField(auto_now_add=True)),
                ('modificacion', models.DateTimeField(auto_now=True)),
                ('usuario', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.DeleteModel(
            name='UsuarioRegistrado',
        ),
    ]
