from django.db import models
from django.contrib.auth.models import User

# Create your models here.
# Modelo extendido o Modelo proxy para el uso del modulo admin de django
# Clase abstracta: Clase que contiene un model y que sus valores no existen; no puede modificarse; cada campo es obligatorio
# Clase abstracta :: User

#class UsuarioRegistrado(models.Model):
#    user = models.OneToOneField(User,on_delete=models.CASCADE)
#    phone = models.CharField(max_length=9, blank=True)
#    foto = models.ImageField(upload_to="registro/imagenes", blank=True, null=True)
#    about = models.TextField(max_length=50, blank=True)
#    created = models.DateTimeField(auto_now_add=True)
#    modified = models.DateTimeField(auto_now=True)

class RegistroMercantil(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    edad = models.CharField(max_length=2)
    telefono = models.CharField(max_length=8)
    estado_civil = models.CharField(max_length=10)
    nacionalidad = models.CharField(max_length=15)
    profesion = models.CharField(max_length=30)
    departamento = models.CharField(max_length=30)
    municipio = models.CharField(max_length=30)
    dpi = models.CharField(max_length=13)
    nit = models.CharField(max_length=10)
    email = models.CharField(max_length=15, blank=True, null=True)
    empresa = models.CharField(max_length=30)
    descripcion = models.TextField(max_length=50)
    creacion = models.DateTimeField(auto_now_add=True)
    modificacion = models.DateTimeField(auto_now=True)

def __str__(self):
    return self.usuario.username