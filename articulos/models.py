from django.db import models

# Create your models here.
class Nacimientos(models.Model):
    nombre = models.CharField(max_length=40)
    apellido = models.CharField(max_length=40)
    lugar_nacimiento = models.CharField(max_length=50)
    nombre_padre = models.CharField(max_length=40)
    nombre_madre = models.CharField(max_length=40)
    fecha_nacimiento = models.DateTimeField()
    fecha_modificacion = models.DateTimeField()

