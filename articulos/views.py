from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime

articulos = [
    {
        'nombre':'Generador de onda',
        'codigo':'G-01',
        'imagen':'https://www.instrumentacionhoy.com/imagenes/2013/07/TFG-3605E.jpg',
        'fecha':datetime.now().strftime('%b %d %Y -%H:%M')
    },
    {
        'nombre':'Osciloscopio',
        'codigo':'O-01',
        'imagen':'https://http2.mlstatic.com/uni-t-osciloscopio-digital-utd2052cl-50mhz-D_NQ_NP_776899-MLA26588059520_012018-F.jpg',
        'fecha':datetime.now().strftime('%b %d %Y -%H:%M')
    },
]

# Create your views here.
#def lista_articulos(request):
#    content = []
#    for articulo in articulos:
#        content.append("""
#            <p><strong>{nombre}</strong></p>
#            <p><small>{codigo} - <i>{fecha}</i></small></p>
#            <figure><img src="{imagen}"/></figure>        
#        """.format(**articulo))
#
#    return HttpResponse('<br>'.join(content))

def index(request):
    return render(request, "index.html", {'articulos':articulos})